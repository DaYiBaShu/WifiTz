package com.dianpingmedia.wifitz.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dianpingmedia.wifitz.WifiService;

/**
 * @author zsc
 * @create 2018/3/21 0021
 * @Describe
 */
public class BootCompletedReceiver extends BroadcastReceiver {
    private boolean run;
    @Override
    public void onReceive(final Context context, Intent intent) {
        if (!run) {
            run = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10000);
                        Intent wifiServiceIntent = new Intent(context, WifiService.class);
                        context.startService(wifiServiceIntent);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

    }
}

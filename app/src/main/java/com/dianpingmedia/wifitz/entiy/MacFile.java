package com.dianpingmedia.wifitz.entiy;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;

/**
 * @author zsc
 * @create 2018/6/4 0004
 * @Describe
 */
@Entity
public class MacFile {
    @Id(autoincrement = true)
    private Long id;
    @NotNull
    private long time;
    @NotNull
    private byte[] file;

    private String wiFiProbeMac;

    private String destMac;

    private String jsonDestMac;

    @Transient
    private int errorTimes;

    @Generated(hash = 869356570)
    public MacFile(Long id, long time, @NotNull byte[] file, String wiFiProbeMac,
            String destMac, String jsonDestMac) {
        this.id = id;
        this.time = time;
        this.file = file;
        this.wiFiProbeMac = wiFiProbeMac;
        this.destMac = destMac;
        this.jsonDestMac = jsonDestMac;
    }

    @Generated(hash = 1140917832)
    public MacFile() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public byte[] getFile() {
        return this.file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public int addErrorTimes() {
        return ++errorTimes;
    }

    public void setErrorTimes(int errorTimes) {
        this.errorTimes = errorTimes;
    }

    @Override
    public String toString() {
        return "MacFile{" +
                "id=" + id +
                ", time=" + time +
                ", wiFiProbeMac='" + wiFiProbeMac + '\'' +
                ", destMac='" + destMac + '\'' +
                '}';
    }

    public int getErrorTimes() {
        return this.errorTimes;
    }

    public String getWiFiProbeMac() {
        return this.wiFiProbeMac;
    }

    public void setWiFiProbeMac(String wiFiProbeMac) {
        this.wiFiProbeMac = wiFiProbeMac;
    }

    public String getDestMac() {
        return this.destMac;
    }

    public void setDestMac(String destMac) {
        this.destMac = destMac;
    }

    public String getJsonDestMac() {
        return this.jsonDestMac;
    }

    public void setJsonDestMac(String jsonDestMac) {
        this.jsonDestMac = jsonDestMac;
    }

}

package com.dianpingmedia.wifitz.entiy;

import java.io.Serializable;

/**
 * @author zsc
 * @create 2018/4/8 0008
 * @Describe Frame 源 MAC|Frame 目的 MAC | Frame 大类|Frame 小类|信道|RSSI 信号强度
 * 60:01:94:77:4B:D0|30:B4:9E:6A:3A:C9|EC:6C:9F:42:E2:30|02|00|9|-69|0|1|0
 * mac地址数据采集表
 */
public class MacCollect implements Serializable {
    private static final long serialVersionUID = -15515456L;
    /**
     * 原mac地址
     */
    private String sourceMac;
    /**
     * 目标mac地址
     */
    private String objectiveMac;
    /**
     * mac大类
     */
    private String frameBig;
    /**
     * mac小类
     */
    private String frameSmall;
    /**
     * 信道
     */
    private String cell;
    /**
     * 信号强度
     */
    private String rssi;
    /**
     * 探测时间
     */
    private String date;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSourceMac() {
        return sourceMac;
    }

    public void setSourceMac(String sourceMac) {
        this.sourceMac = sourceMac;
    }

    public String getObjectiveMac() {
        return objectiveMac;
    }

    public void setObjectiveMac(String objectiveMac) {
        this.objectiveMac = objectiveMac;
    }

    public String getFrameBig() {
        return frameBig;
    }

    public void setFrameBig(String frameBig) {
        this.frameBig = frameBig;
    }

    public String getFrameSmall() {
        return frameSmall;
    }

    public void setFrameSmall(String frameSmall) {
        this.frameSmall = frameSmall;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MacCollect{" +
                "sourceMac='" + sourceMac + '\'' +
                ", objectiveMac='" + objectiveMac + '\'' +
                ", frameBig='" + frameBig + '\'' +
                ", frameSmall='" + frameSmall + '\'' +
                ", cell='" + cell + '\'' +
                ", rssi='" + rssi + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}

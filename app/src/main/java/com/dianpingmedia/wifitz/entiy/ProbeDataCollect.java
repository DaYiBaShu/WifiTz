package com.dianpingmedia.wifitz.entiy;

import android.text.TextUtils;

import com.dianpingmedia.wifitz.utlis.SPUtils;
import com.google.gson.Gson;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author zsc
 * @create 2018/4/8 0008
 * @Describe Frame 源 MAC|Frame 目的 MAC | Frame 大类|Frame 小类|信道|RSSI 信号强度
 * 60:01:94:77:4B:D0|30:B4:9E:6A:3A:C9|EC:6C:9F:42:E2:30|02|00|9|-69|0|1|0
 * mac地址数据采集表
 */
public class ProbeDataCollect implements Serializable {
    private int length;
    private ProbeDataInit mProbeDataInit = new ProbeDataInit();
    private long createTime = System.currentTimeMillis();
    private int maximum = SPUtils.getUnit() * 60 * 1000;

    public void updateTime() {
        this.maximum = SPUtils.getUnit() * 60 * 1000;
        this.createTime = System.currentTimeMillis();
        length = 0;
    }

    public boolean isTimeout() {
        if (System.currentTimeMillis() >= createTime + maximum) {
            return true;
        }
        return false;
    }

    public boolean isOutOfSize(int size) {
        return length >= size;
    }

    public String destMacsToJson() {
        return mProbeDataInit.destMacsToJson();
    }


    public static class ProbeData implements Serializable {
        /**
         * 原mac地址
         */
        private String m_strSrcMac;

        private String m_strObjMac;

        private String m_strWiFiProbeMac;


        /**
         * mac大类
         */
        private int m_bySortBig;

        /**
         * mac小类
         */
        private int m_bySortSmall;

        /**
         * 信道
         */
        private int m_iChannel;

        /**
         * 信号强度
         */
        private int m_iSignalAttenuation;

        /**
         * 探测时间，仅传送时分秒，格式HHmmss
         */
        private int m_strTime;

        public String getM_strObjMac() {
            return m_strObjMac;
        }

        public void setM_strObjMac(String m_strObjMac) {
            this.m_strObjMac = m_strObjMac;
        }

        public String getM_strSrcMac() {
            return m_strSrcMac;
        }

        public void setM_strSrcMac(String m_strSrcMac) {
            this.m_strSrcMac = m_strSrcMac;
        }

        public int getM_bySortBig() {
            return m_bySortBig;
        }

        public void setM_bySortBig(int m_bySortBig) {
            this.m_bySortBig = m_bySortBig;
        }

        public int getM_bySortSmall() {
            return m_bySortSmall;
        }

        public void setM_bySortSmall(int m_bySortSmall) {
            this.m_bySortSmall = m_bySortSmall;
        }

        public int getM_iChannel() {
            return m_iChannel;
        }

        public void setM_iChannel(int m_iChannel) {
            this.m_iChannel = m_iChannel;
        }

        public String getM_strWiFiProbeMac() {
            return m_strWiFiProbeMac;
        }

        public void setM_strWiFiProbeMac(String m_strWiFiProbeMac) {
            this.m_strWiFiProbeMac = m_strWiFiProbeMac;
        }

        public int getM_iCellPower() {
            return (this.m_iSignalAttenuation - 100);
        }

        public void setM_iCellPower(int m_iCellPower) {
            this.m_iSignalAttenuation = m_iCellPower + 100;
        }

        public int getM_strTime() {
            return m_strTime;
        }

        public void setM_strTime(int m_strTime) {
            this.m_strTime = m_strTime;
        }

        public ProbeData() {

        }

        public ProbeData(String m_strSrcMac, byte m_bySortBig, byte m_bySortSmall, byte m_iChannel, int m_iCellPower, int m_strTime) {
            this.m_strSrcMac = m_strSrcMac;
            this.m_bySortBig = m_bySortBig;
            this.m_bySortSmall = m_bySortSmall;
            this.m_iChannel = m_iChannel;
            this.m_iSignalAttenuation = m_iCellPower;
            this.m_strTime = m_strTime;
        }

        @Override
        public String toString() {
            return this.m_strSrcMac + ","
                    + this.m_bySortBig + ","
                    + this.m_bySortSmall + ","
                    + this.m_iChannel + ","
                    + this.m_iSignalAttenuation + ","
                    + this.m_strTime;
        }
    }    //Class End

    private StringBuilder m_strProbeDataSet = new StringBuilder();

    public int length() {
        return this.m_strProbeDataSet.length();
    }

    public void clear() {
        length = 0;
        this.m_strProbeDataSet.setLength(0);
        mProbeDataInit.clear();
    }

    /**
     * 加入一个ProbeData对象。这个对象可以再次被赋值，然后再次添加，不需要重复new新的对象。
     *
     * @param pd
     */
    public void add(ProbeData pd) {
        length++;
        if (this.m_strProbeDataSet.length() > 1) {
            this.m_strProbeDataSet.append(";");
        }
        this.m_strProbeDataSet.append(pd.toString());
    }

    @Override
    public String toString() {
        return this.m_strProbeDataSet.toString();
    }

    public int getSize() {
        return length;
    }

    public void setM_strDate(String m_strDate) {
        mProbeDataInit.setM_strDate(m_strDate);
    }

    public void setM_strWiFiProbeMac(String m_strWiFiProbeMac) {
        mProbeDataInit.setM_strWiFiProbeMac(m_strWiFiProbeMac);
    }

    public void addStrObjMac(String strMac) {
        mProbeDataInit.add(strMac);
    }

    public ProbeDataInit getProbeDataInit() {
        return mProbeDataInit;
    }
}

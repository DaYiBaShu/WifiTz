package com.dianpingmedia.wifitz.entiy;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

public class ProbeDataInit {
    /**
     * 之后传送的数据的日期，格式为年月日，"YYYYMMDD"
     * 在没有接收到下一个Init命令之前，所传送的数据都是在这个日期下的具体时间
     */
    private String m_strDate = "";

    /**
     * WiFi探针的MAC地址
     */
    private String m_strWiFiProbeMac = "";
    private static final Gson mGson = new Gson();
    private static final Type mType = new TypeToken<List<String>>() {}.getType();

    public void setM_strDate(String m_strDate) {
        if (TextUtils.isEmpty(this.m_strDate))
            this.m_strDate = m_strDate;
    }

    public void setM_strWiFiProbeMac(String m_strWiFiProbeMac) {
        if (TextUtils.isEmpty(this.m_strWiFiProbeMac))
            this.m_strWiFiProbeMac = m_strWiFiProbeMac;
    }

    public void setM_strDestMac(String m_strDestMac) {
        this.m_strDestMac = m_strDestMac;
    }

    public boolean add(String strMac) {
        if (!this.destMacs.contains(strMac)) {
            if (!TextUtils.isEmpty(this.m_strDestMac)) {
                this.m_strDestMac = this.m_strDestMac + ",";
            }
            this.m_strDestMac = this.m_strDestMac + strMac;
            destMacs.add(strMac);
            return true;
        }
        return false;
    }

    public String getM_strWiFiProbeMac() {
        return m_strWiFiProbeMac;
    }

    public String getM_strDestMac() {
        return m_strDestMac;
    }

    /**
     * 目标MAC地址，一般为周边的路由设备或WiFi接收设备的MAC地址
     */
    private String m_strDestMac = "";

    private List<String> destMacs = new ArrayList<String>();

    //    private Map<String, Integer> m_mapDestMac = new HashMap<String, Integer>();

    public void clear() {
        this.m_strDestMac = "";
        this.m_strDate = "";
        this.destMacs.clear();
    }

    public String destMacsToJson() {
        return mGson.toJson(destMacs);
    }

    @Override
    public String toString() {
        return "{\"Date\":\"" + this.m_strDate
                + "\",\"ProbeMac\":\"" + this.m_strWiFiProbeMac
                + "\",\"DestMac\":\"" + this.m_strDestMac
                + "\"}";
    }

    public String getM_strDate() {
        return m_strDate;
    }

    public void setDestMacsJson(String json) {
        this.destMacs = mGson.fromJson(json, mType);
    }

    public List<String> getDestMacs() {
        return destMacs;
    }
}

package com.dianpingmedia.wifitz.entiy;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * @author zsc
 * @create 2018/6/12 0012
 * @Describe
 */
@Entity
public class WifiTzData {
    @Id(autoincrement = true)
    private Long id;
    private String mac;
    private int major;
    private int minor;
    private int channel;
    private int power;
    private String meter;
    private String time;
    @Generated(hash = 676284158)
    public WifiTzData(Long id, String mac, int major, int minor, int channel,
            int power, String meter, String time) {
        this.id = id;
        this.mac = mac;
        this.major = major;
        this.minor = minor;
        this.channel = channel;
        this.power = power;
        this.meter = meter;
        this.time = time;
    }
    @Generated(hash = 188824962)
    public WifiTzData() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getMac() {
        return this.mac;
    }
    public void setMac(String mac) {
        this.mac = mac;
    }
    public int getMajor() {
        return this.major;
    }
    public void setMajor(int major) {
        this.major = major;
    }
    public int getMinor() {
        return this.minor;
    }
    public void setMinor(int minor) {
        this.minor = minor;
    }
    public int getChannel() {
        return this.channel;
    }
    public void setChannel(int channel) {
        this.channel = channel;
    }
    public int getPower() {
        return this.power;
    }
    public void setPower(int power) {
        this.power = power;
    }
    public String getMeter() {
        return this.meter;
    }
    public void setMeter(String meter) {
        this.meter = meter;
    }
    public String getTime() {
        return this.time;
    }
    public void setTime(String time) {
        this.time = time;
    }
 
}


package com.dianpingmedia.wifitz;

import android.app.Application;

import com.dianpingmedia.wifitz.read.manager.ReadManager;
import com.dianpingmedia.wifitz.read.usb.UsbReadManager;
import com.dianpingmedia.wifitz.utlis.SPUtils;

/**
 * @author zsc
 * @create 2018/4/4 0004
 * @Describe
 */
public class WifiTzApplication extends Application {
    private static WifiTzApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        SPUtils.init(this);
        UsbReadManager.init(this);
    }

    public static WifiTzApplication getInstance() {
        return instance;
    }
}



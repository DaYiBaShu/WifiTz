package com.dianpingmedia.wifitz.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.dianpingmedia.wifitz.entiy.WifiTzData;
import com.dianpingmedia.wifitz.read.listener.OnOutputListener;
import com.dianpingmedia.wifitz.read.usb.UsbReadManager;
import com.dianpingmedia.wifitz.utlis.RssiMeterUtil;
import com.dianpingmedia.wifitz.utlis.SPUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author zsc
 * @create 2018/5/18 0018
 * @Describe
 */
public class Test extends AppCompatActivity {


    //    private OkHttpClient mClient = new OkHttpClient().newBuilder()
    //            .connectTimeout(10, TimeUnit.SECONDS)
    //            .readTimeout(10, TimeUnit.SECONDS)
    //            .writeTimeout(10, TimeUnit.SECONDS)//设置写的超时时间
    //            .build();
    //
    //    private String url = "http://rtb.test.voiceads.cn/view?info=CJ7Yj9gFEEEaJGRlMmYzODdiLTg0NmQtNDY4Zi04NDJlLTU4MjhjZWYxMWY3OSIkNGM4YmQ5NjQtNmJkYi00MDNkLWJlOWYtNTc0NmQwNDM2N2MwKJ-U1m0wgZc9OKWKjQFAselaSOznjQFaIEY0NzRBRTI5QkU5RkRENkRBRDEyNzlCMzAxNThBMjVDZQAAAABtAAAAAHAAeACAAYDC1y-IAQGSATJmNzE3ZTExZS01MGRjLTQzMDgtODg0NC0xZmIyZjNiOGI2MGYtMTUyNjk4MzcxMTY5OJgBkBygAQA&wp=bvmFEpxiu6HWXEswvc7I6w";
    //    private String url2 = "http://192.168.0.28:8080/testnotices?a=1";

    private static Handler mSaveHandler;

    static {
        HandlerThread handlerThread = new HandlerThread("Test");
        handlerThread.start();
        mSaveHandler = new Handler(handlerThread.getLooper());
    }

    private final static String TAG = "Test";
    private static final SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UsbReadManager usbReadManager = UsbReadManager.getInstance();
        usbReadManager.addOnOutputListener(new OnOutputListener() {
            @Override
            public void onSuccess(final String data) {
                Log.d(TAG + "-onSuccess:", data);
                mSaveHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        parse(data);
                    }
                });
            }

            @Override
            public void onFail(String failMsg) {
                Log.d(TAG + "-onFail:", failMsg);
            }
        });
        usbReadManager.start(2);
    }

    private void parse(String data) {
        String[] macList = data.split("\n");
        for (String macStr : macList) {
            WifiTzData wifiTzData = null;
            try {
                wifiTzData = new WifiTzData();
                String[] split = macStr.split("\\|");
                wifiTzData.setTime(mFormat.format(new Date()));
                wifiTzData.setMac(split[1]);
                String sortBigStr = split[3];
                String bySortSmallStr = split[4];
                int sortBig = Integer.parseInt(sortBigStr, 16);
                int sortSmall = Integer.parseInt(bySortSmallStr, 16);
                wifiTzData.setMajor(sortBig);
                wifiTzData.setMinor(sortSmall);
                wifiTzData.setChannel(Integer.parseInt(split[5]));
                String rssi = split[6];
                wifiTzData.setPower(Integer.parseInt(rssi));
                if (!RssiMeterUtil.within(rssi, SPUtils.getRang())) {
                    String msg = "信号强度不在范围内对丢弃处理:" + wifiTzData;
                    Log.e(TAG, msg);
                    continue;
                }
                Log.i(TAG, wifiTzData.toString());
                // 可入库

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "数据解析失败:" + macStr);
            }
        }
    }

}

package com.dianpingmedia.wifitz.tcp.dataObjectDef;

import android.util.Log;

import java.util.Calendar;
import java.util.Date;

public class ConvertLib 
{
	
	private static long getNextDay(Date date, int iHour, int iMinute, int iSecond ) 
	{
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +1);//+1今天的时间加一天
        calendar.set(Calendar.HOUR_OF_DAY, iHour);
        calendar.set(Calendar.MINUTE, iMinute);
        calendar.set(Calendar.SECOND, iSecond);
        return calendar.getTimeInMillis();
    }

	public static long getTodayEndTime() 
	{  
        Calendar todayEnd = Calendar.getInstance();  
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);  
        todayEnd.set(Calendar.MINUTE, 59);  
        todayEnd.set(Calendar.SECOND, 59);  
        todayEnd.set(Calendar.MILLISECOND, 999);  
        return todayEnd.getTimeInMillis();  
    }

	/**
	 * 得到日期的YYYYMMDD的字符串表示
	 * @param strDate
	 * @return
	 */
	public static String getYYYYMMDD(String strDate)
	{
		return (strDate.substring(0,4) + strDate.substring(5, 7) + strDate.substring(8, 10));
	}
	
	
	/**
	 * 转变读取到的byte[]为长度。本方法适用于网络字节序(大端字节序Big Endian，低位在前，高位在后)的顺序，和和intToBytesByBigEndian（）配套使用
	 * @param byArray			字节数组
	 * @param iIntByteCount		要转换的长度的字节表示的个数
	 * @return	长度
	 */
	public static int bytesToIntByBigEndian( byte[] byArray, int iIntByteCount )
	{
		int iInt = 0;
		--iIntByteCount;
		for( int i = 0; i <= iIntByteCount; ++i )
		{
			iInt = ( iInt << 8 ) + ( byArray[iIntByteCount - i] & 0xFF );
		}
		return iInt;
	}
	
	/**
	 * 转变读取到的byte[]为长度。本方法适用于小端字节序(Little Endian，低位在后，高位在前)的顺序，和和intToBytesByLittleEndian（）配套使用
	 * @param byArray			字节数组
	 * @param iIntByteCount		要转换的长度的字节表示的个数
	 * @return	长度
	 */
	public static int bytesToIntByLittleEndian( byte[] byArray, int iIntByteCount )
	{
		int iInt = 0;
		for( int i = 0; i < iIntByteCount; ++i )
		{
			iInt = ( iInt << 8 ) + ( byArray[i] & 0xFF );
		}
		return iInt;
	}
	
	/**
	 * 将iLength的int值转变成byte[]。本方法适用于大端字节序(Big Endian，低位在前，高位在后)的顺序，和和bytesToIntByBigEndian（）配套使用
	 * @param iInt				要转变的int值
	 * @param iIntByteCount		int所占的字节个数，值可以是1，2，3，4其中之一
	 * @param byDest			保存转换后的byte[]的目标数组
	 * @param iOffset			在目标数组中的偏移
	 * @return					转换后的byte[]目标数组
	 */
	/**
	 * 
	 * @return
	 */
	public static byte[] intToBytesByBigEndian( int iInt, int iIntByteCount, byte[] byDest, int iOffset )
	{
        Log.d("ConvertLib","----------");
		for( int i = 0; i < iIntByteCount; ++i )
		{
			byDest[iOffset + i] = (byte)iInt;
			iInt = ( iInt >> 8 );
		}
		return byDest;
	}	
	
	/**
	 * 将iLength的int值转变成byte[]。本方法适用于端字节序(Little Endian，低位在后，高位在前)的顺序，和和bytesToIntByLittleEndian（）配套使用
	 * @param iInt				要转变的int值
	 * @param iIntByteCount		int所占的字节个数，值可以是1，2，3，4其中之一
	 * @param byDest			保存转换后的byte[]的目标数组
	 * @param iOffset			在目标数组中的偏移
	 * @return					转换后的byte[]目标数组
	 */
	public static byte[] intToBytesByLittleEndian( int iInt, int iIntByteCount, byte[] byDest, int iOffset )
	{
		for( int i = iIntByteCount - 1; i >= 0; --i )
		{
			byDest[iOffset + i] = (byte)iInt;
			iInt = ( iInt >> 8 );
		}
		return byDest;
	}	
}

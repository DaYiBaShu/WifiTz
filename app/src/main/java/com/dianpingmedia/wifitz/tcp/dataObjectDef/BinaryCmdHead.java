package com.dianpingmedia.wifitz.tcp.dataObjectDef;


/**
 * 二进制命令，开始的
 *
 * @author 13925
 */
public class BinaryCmdHead {
    public static final int C_CMD_WIFI_PROBE_SEND_DATA = 1;
    public static final int C_CMD_WIFI_PROBE_SEND_NEXT_DATA = 2;
    public static final int C_CMD_WIFI_PROBE_ZIP_ERROR = 3;
    public static final int C_CMD_WIFI_PROBE_SEND_INIT = 4;
    private static final int C_CMD_CODE_BYTE_LENGTH = 2;
    public int m_iCmd;
    private byte[] m_byArray = null;
    private int m_iDataSize = 0;

    public BinaryCmdHead() {

    }

    public BinaryCmdHead(int iCmd, byte[] byArrayData, int iDataSize) {
        this.m_iCmd = iCmd;
        this.setBinaryArray(byArrayData, iDataSize);
    }

    public byte[] getBytesCopy() {
        int iSize = this.getBytesSize();
        byte[] byArray = new byte[iSize];
        System.arraycopy(this.getBytes(), 0, byArray, 0, iSize);
        return byArray;
    }

    public byte[] getBytes() {
        return this.m_byArray;
    }

    public int getBytesSize() {
        return (this.isOnlyCmd() ? BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH : this.m_iDataSize + BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH);
    }

    /**
     * 是否只有命令。
     *
     * @return
     */
    public boolean isOnlyCmd() {
        return (this.m_iDataSize == 0);
    }

    /**
     * 设置命令号
     *
     * @param iCmd 命令编号，值应该在0..65535之间
     */
    public void setCmd(int iCmd, boolean bOnlyCmd) {
        this.m_iCmd = iCmd;
        if (this.m_byArray == null) {
            this.m_byArray = new byte[BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH];
        }
        ConvertLib.intToBytesByBigEndian(iCmd, BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH, this.m_byArray, 0);
        if (bOnlyCmd) {
            this.m_iDataSize = 0;
        }
    }

    public void setBinaryArray(byte[] byArrayData, int iDataSize) {
        this.m_iDataSize = iDataSize;
        iDataSize += BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH;
        if (this.m_byArray == null || this.m_byArray.length < iDataSize) {
            this.m_byArray = new byte[iDataSize];
        }
        ConvertLib.intToBytesByBigEndian(this.m_iCmd, BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH, this.m_byArray, 0);
        System.arraycopy(byArrayData, 0, this.m_byArray, BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH, this.m_iDataSize);
    }

    public boolean parse(byte[] byArrayData, int iDataSize, boolean bCopy) {
        try {
            //读取到了长度
            this.m_iCmd = ConvertLib.bytesToIntByBigEndian(byArrayData, BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH);

            this.m_iDataSize = iDataSize - BinaryCmdHead.C_CMD_CODE_BYTE_LENGTH;
            if (bCopy) {
                if (this.m_byArray == null || this.m_byArray.length < iDataSize) {
                    this.m_byArray = new byte[iDataSize];
                }
                System.arraycopy(byArrayData, 0, this.m_byArray, 0, iDataSize);
            } else {
                this.m_byArray = byArrayData;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String toString() {
        return (this.m_byArray == null ? "" : new String(this.m_byArray, 0, this.m_iDataSize));
    }

    public int getCmd() {
        return m_iCmd;
    }
}

package com.dianpingmedia.wifitz.tcp.dataObjectDef;

public class CmdDef 
{
	/**
	 * 命令号的由8个0-9的数字组成，高4个数字表示协议号，低4个数字表示具体的命令号。
	 */
	public static final int	S_CMD_INVALID					= 00000000;		//无效的命令
	
	/**
	 * 审核通过、拒绝，或者在接收到S_CMD_GET_ALL_VALID_MATERIALS命令时，由素材审核子系统发送该命令。
	 * 该命令包含一个素材所对应的审核（或拒绝）的屏的所有ID。也可以分开成多个命令发送，每次发送部分ID。
	 * 如果是在接收到S_CMD_GET_ALL_VALID_MATERIALS命令时，则查找所有的有效的素材，然后依次发送JsonCmdAudit对象给远端。
	 * 		这时命令中可以只发送审核通过的屏的ID，拒绝的不需要发送。
	 * 
	 * 业务子系统接收到该命令时的处理：
	 * 如果是审核通过，则查找每一个屏所对应的ScrrenInfo，然后将素材ID增加到该屏的允许播放的素材Map中。
	 * 如果是拒绝通过，则查找每一个屏所对应的ScrrenInfo，然后将素材ID从该屏的允许播放的素材Map中删除。
	 */
	public static final int	S_CMD_AUDIT_MATERIAL			= 10000100;		//对应JsonCmdAudit类

	/**
	 * 传送素材的信息。
	 * 在素材审核子系统中接收到该命令时，找到指令素材的相关信息，填充Json对象类，然后发送给远端。
	 * 
	 * 业务子系统接收到该命令时的处理：
	 * 如果素材的ID不存在，则新增；
	 * 如果素材的ID已经存，则更新。
	 */
	public static final int	S_CMD_GET_MATERIAL_INFO			= 10000101;		//对应JsonCmdAuditMaterial类

	/**
	 * 素材审核子系统接收到该命令时，找出所有有效地素材，填充JsonCmdAudit对象，依次发送给远端。
	 */
	public static final int	S_CMD_GET_ALL_VALID_MATERIALS	= 10000102;		//得到所有有效的素材。没有内容

}

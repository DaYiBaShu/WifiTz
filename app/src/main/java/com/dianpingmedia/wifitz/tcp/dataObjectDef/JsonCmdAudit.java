package com.dianpingmedia.wifitz.tcp.dataObjectDef;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 当一个媒体商审核通过或审核拒绝一个素材时，发送该命令。
 * 如果媒体商对一个素材的审核拒绝是第一次发出，即原来素材的状态为待审核状态时，可以不用发送该指令。
 * 如果媒体商对一个素材的审核状态从审核通过变为审核拒绝状态时，必需要发出该指令。
 * 在该命令的ScreenId中，可以包含多个屏的ID。但要注意总的JSON序列化字符串的长度，不能太长，最好控制一下数量。该命令可以多次发出。
 * 审核通过和审核拒绝的列表可以同时存在，也可以只存在任意一个。
 * 
 * 该命令对象在以下几种情况下会发出或接收到：
 * 1　发送方：素材审核子系统。
 * 	     接收方：业务子系统。
 *    处理：    在素材审核子系统接收到媒体商的审核状态发生改变时，由素材审核子系统向业务子系统主动发送。
 *    
 * 2　发送方：业务子系统。
 *    接收方：素材审核子系统。
 *    处理：     当素材审核处理子系统收到该命令时，如果m_lMaterialId>=0时，表示需要填充指定素材的审核通过或审核拒绝的ScreenId列表。
 *            当素材审核处理子系统收到该命令时，如果m_lMaterialId<0时，表示需要循环发送所有当前有效的素材的状态和屏列表。
 * @author 13925
 *
 */
public class JsonCmdAudit 
{
	/**
	 * 素材ID
	 */
	public	long			m_lMaterialId;
	
	/**
	 * 审核通过的该素材的ScreenId的列表。
	 * 如果是拒绝，则该值可以忽略。
	 */
	private	List<String>	m_listAgreeScreenId = new ArrayList<String>();
	
	/**
	 * 审核拒绝的该素材的ScreenId的列表。
	 * 如果是审核通过，则该值可以忽略。
	 */
	private List<String>	m_listRefuseScreenId = new ArrayList<String>();
	
	
	public void addAgreeScreenId(String strScreenId)
	{
		this.m_listAgreeScreenId.add(strScreenId);
	}
	
	public void addRefuseScreenId(String strScreenId)
	{
		this.m_listRefuseScreenId.add(strScreenId);
	}
	
	public List<String>	getAgreeList()
	{
		return this.m_listAgreeScreenId;
	}
	
	public List<String> getRefuseList()
	{
		return this.m_listRefuseScreenId;
	}
	
	/**
	 * 将JSON字符串解析成对象
	 * @param strJson
	 * @return
	 */
	public boolean parse(String strJson)
	{
		try
		{
			JSONObject json = new JSONObject(strJson);
			return this.parse(json);
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	public boolean parse(JSONObject json)
	{
		try
		{
			this.m_lMaterialId = json.getLong("m_id");
			
			try
			{
				//Agree列表
				JSONArray jsonArray = json.getJSONArray("Agree");
				int iSize = jsonArray.length();
				for( int i = 0; i < iSize; ++i )
				{
					this.addAgreeScreenId(jsonArray.getString(i));
				}
			}
			catch(Exception e)
			{
			}
			
			try
			{
				//Refuse列表
				JSONArray jsonArray = json.getJSONArray("Refuse");
				int iSize = jsonArray.length();
				for( int i = 0; i < iSize; ++i )
				{
					this.addRefuseScreenId(jsonArray.getString(i));
				}
			}
			catch(Exception e)
			{
			}
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	/**
	 * 序列化成JSON字符串
	 */
	@Override  
    public String toString() 
	{  
		String strAgree = "";
		int iSize = this.m_listAgreeScreenId.size();
		for( int i = 0; i < iSize; ++i )
		{
			strAgree += ",\"" + this.m_listAgreeScreenId.get(i) + "\"";
		}
		
		String strRefuse = "";
		iSize = this.m_listRefuseScreenId.size();
		for( int i = 0; i < iSize; ++i )
		{
			strRefuse += ",\"" + this.m_listRefuseScreenId.get(i) + "\"";
		}
		
		if( strAgree.length() > 1 )
		{
			strAgree = ",\"Agree\":[" + strAgree.substring(1) + "]";
		}
		
		if( strRefuse.length() > 1 )
		{
			strRefuse = ",\"Refuse\":[" + strRefuse.substring(1) + "]";
		}
        return ( "{\"m_id\":" + this.m_lMaterialId + strAgree + strRefuse + "}" );
	}
}

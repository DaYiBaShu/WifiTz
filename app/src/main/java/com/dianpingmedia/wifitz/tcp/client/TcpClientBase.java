package com.dianpingmedia.wifitz.tcp.client;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.dianpingmedia.wifitz.entiy.ProbeDataInit;
import com.dianpingmedia.wifitz.utlis.SPUtils;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class TcpClientBase extends MyTcpBase {
    private static final String TAG = "TcpClientBase";
    private String mIp = SPUtils.getIp();
    private int mPort = SPUtils.getPort();
    private Handler mLogHandler;
    private ProbeDataInit mProbeDataInit = null;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss--", Locale.getDefault());


    public boolean connect() {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(mIp = SPUtils.getIp(), mPort = SPUtils.getPort()), 5000);
            if (this.attachSocket(socket)) {
                return true;
            }
        } catch (Exception e) {
            sendLog(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return false;
    }


    public boolean isChange() {
        return !mIp.equals(SPUtils.getIp()) || mPort != SPUtils.getPort();
    }


    public String getIp() {
        return mIp;
    }

    public int getPort() {
        return mPort;
    }

    public void setHandler(Handler handler) {
        mLogHandler = handler;
    }


    private void sendLog(String log) {
        Log.d(TAG, log);
        if (mLogHandler != null) {
            Message obtain = Message.obtain();
            obtain.what = 1;
            obtain.obj = getCurrTime() + log;
            mLogHandler.sendMessage(obtain);
        }
    }

    private String getCurrTime() {
        return mDateFormat.format(new Date());
    }

    public boolean isConnect() {
        try {
            return getSocket().isConnected();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean equalsProbeDataInit(ProbeDataInit probeDataInit) {
        try {
            if (this.mProbeDataInit == null) {
                this.mProbeDataInit = probeDataInit;
                return false;
            }
            if (!probeDataInit.getM_strDate().equals(this.mProbeDataInit.getM_strDate())) {
                this.mProbeDataInit = probeDataInit;
                return false;
            }
            List<String> destMacs = this.mProbeDataInit.getDestMacs();
            List<String> contrastDestMacs = probeDataInit.getDestMacs();
            if (destMacs != null && contrastDestMacs != null) {
                if (destMacs.size() < contrastDestMacs.size()) {
                    this.mProbeDataInit = probeDataInit;
                    return false;
                }
                if (!destMacs.containsAll(contrastDestMacs)) {
                    this.mProbeDataInit = probeDataInit;
                    return false;
                }
            }
        } catch (Exception e) {
        }
        return true;
    }

    @Override
    public void close() {
        super.close();
        this.mProbeDataInit = null;
    }

    public ProbeDataInit getProbeDataInit() {
        return mProbeDataInit;
    }
}

package com.dianpingmedia.wifitz.utlis;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * @author zsc
 * @create 2018/4/4 0004
 * @Describe
 */
public class SPUtils {
    private static SharedPreferences mSp;
    private final static String SHARED_NAME = "wifitz";
    private final static String USBTTY = "usbtty";
    private final static String IP = "ip";
    private final static String PORT = "port";
    private final static String UNIT = "unit";
    private final static String RANG = "rang";


    private static String DEFAULT_USBTTY = "/dev/ttyUSB0";
    private final static String DEFAULT_IP = "192.168.100.48";
    private final static int DEFAULT_PORT = 1279;
    private final static int DEFAULT_UNIT = 1;
    private final static int DEFAULT_RANG = 100;
    private final static int COUNT = 100;

    public static void init(Context context) {
        mSp = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        if (TextUtils.isEmpty(getUsbtty())) {
            try {
                DEFAULT_USBTTY = new SerialPortFinder().getAllDevicesPath()[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void saveIp(String ip) {
        try {
            SharedPreferences.Editor edit = mSp.edit();
            edit.putString(IP, ip);
            edit.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void savePort(String port) {
        try {
            SharedPreferences.Editor edit = mSp.edit();
            edit.putInt(PORT, Integer.parseInt(port));
            edit.apply();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public static void saveUnit(String unit) {
        try {
            SharedPreferences.Editor edit = mSp.edit();
            edit.putInt(UNIT, Integer.parseInt(unit));
            edit.apply();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public static void saveUsbtty(String usbtty) {
        try {
            SharedPreferences.Editor edit = mSp.edit();
            edit.putString(USBTTY, usbtty);
            edit.apply();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public static void saveRang(String rang) {
        try {
            SharedPreferences.Editor edit = mSp.edit();
            edit.putInt(RANG, Integer.valueOf(rang));
            edit.apply();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public static String getIp() {
        return mSp.getString(IP, DEFAULT_IP);
    }

    public static int getPort() {
        return mSp.getInt(PORT, DEFAULT_PORT);
    }

    public static int getUnit() {
        return mSp.getInt(UNIT, DEFAULT_UNIT);
    }

    public static Integer getRang() {
        return mSp.getInt(RANG, DEFAULT_RANG);
    }


    public static String getUsbtty() {
        return mSp.getString(USBTTY, DEFAULT_USBTTY);
    }

    public static int getCount() {
        return COUNT;
    }
}

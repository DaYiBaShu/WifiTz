package com.dianpingmedia.wifitz.utlis;

import android.util.Log;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author zsc
 * @create 2018/4/23 0023
 * @Describe
 */
public class RssiMeterUtil {

    private final static int P = -50;
    private final static int N = 3;

    public static float rssiToMeter(int rssi) {
        DecimalFormat fnum = new DecimalFormat("##0.0");
        double pow = Math.pow(10, (float) ((double) ((P - rssi)) / (10 * N)));
        return Float.parseFloat(fnum.format(pow));
    }

    public static int meterToRssi(float meter) {
        return (int) Math.round((P - (Math.log(meter) / Math.log(10)) * (10 * N)));
    }

    public static boolean within(String rssi, int spMeter) {
        int rssiInt = Integer.parseInt(rssi);
        float meter = rssiToMeter(rssiInt);
        return meter <= spMeter;
    }
}

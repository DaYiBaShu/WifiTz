package com.dianpingmedia.wifitz.utlis;

/**
 * @author zsc
 * @create 2018/6/7 0007
 * @Describe
 */
public class StringUtil {

    public static String omitStr(String str, int length) {
        if (str.length() > length) {
            return str.substring(0, length) + "......";
        } else {
            return str;
        }
    }
}

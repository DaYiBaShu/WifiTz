package com.dianpingmedia.wifitz.utlis;

import com.dianpingmedia.wifitz.entiy.MacCollect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zsc
 * @create 2018/6/4 0004
 * @Describe
 */
public class MacArrayList implements Serializable {
    private long createTime = System.currentTimeMillis();
    private int maximum = SPUtils.getUnit() * 60 * 1000;
    private ArrayList<MacCollect> mMacCollects = new ArrayList<MacCollect>();

    public MacArrayList() {
        super();
    }

    public void updateTime() {
        this.maximum = SPUtils.getUnit() * 60 * 1000;
        this.createTime = System.currentTimeMillis();
    }

    public boolean isTimeout() {
        if (System.currentTimeMillis() >= createTime + maximum) {
            return true;
        }
        return false;
    }

    public boolean addAll(List<MacCollect> macCollect) {
        return mMacCollects.addAll(macCollect);
    }

    public void clear() {
        mMacCollects.clear();
    }

    public ArrayList<MacCollect> getMacCollects() {
        return mMacCollects;
    }
}

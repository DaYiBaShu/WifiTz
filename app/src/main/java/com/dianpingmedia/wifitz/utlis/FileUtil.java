package com.dianpingmedia.wifitz.utlis;
import android.content.Context;
import android.util.Log;

import com.dianpingmedia.wifitz.WifiTzApplication;
import com.dianpingmedia.wifitz.entiy.MacCollect;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.types.FileSet;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.zip.Deflater;

/**
 * @author zsc
 * @create 2018/6/4 0004
 * @Describe
 */
public class FileUtil {
    private static String parentPath;

    static {
        parentPath = WifiTzApplication.getInstance().getCacheDir().getAbsolutePath() + File.separator + "tzData";
        File file = new File(parentPath);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    /**
     * @param bean
     * @return
     */
    public static File writeObjectToZip(Context context,  List<MacCollect> bean) {
        File file = null;
        File zip = null;
        try {
            file = writeObject(context,bean);
            if (file != null) {
                Log.d("FileUtil", "压缩前大小:" + file.length() / 1024);
                zip = createZip(file.getAbsolutePath(), file.getAbsolutePath().replace(".obj", "") + ".zip");
                file.delete();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return zip;
    }

    private static File writeObject(Context context, List<MacCollect> bean) throws IOException {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        String savePath = parentPath + File.separator + System.currentTimeMillis() + ".obj";
        File file = new File(savePath);
        try {
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(bean);
            Log.i("FileUtil", "写入对象成功");
            return file;
        } catch (FileNotFoundException e) {
            Log.i("FileUtil", "写入对象失败");
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            Log.i("FileUtil", "写入对象失败");
            e.printStackTrace();
            return null;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static File createZip(String srcPathName, String finalFile) {
        File zipFile = new File(finalFile);
        File srcdir = new File(srcPathName);
        if (!srcdir.exists()) {
            throw new RuntimeException(srcPathName + "不存在！");
        }
        Project prj = new Project();
        FileSet fileSet = new FileSet();
        fileSet.setProject(prj);
        fileSet.setFile(srcdir);
        Zip zip = new Zip();
        zip.setLevel(Deflater.BEST_COMPRESSION);
        zip.setProject(prj);
        zip.setDestFile(zipFile);
        zip.setEncoding("gbk");
        zip.addFileset(fileSet);
        zip.execute();
        Log.d("FileUtil", "压缩后大小:" + zipFile.length() / 1024);
        return zipFile;
    }


    public static byte[] fileToBinary(File file) {
        byte[] buffer = null;
        FileInputStream fis = null;
        ByteArrayOutputStream bos = null;
        try {
            fis = new FileInputStream(file);
            bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            buffer = bos.toByteArray();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                if (null != bos) {
                    bos.close();
                }
            } catch (IOException ex) {
            } finally {
                try {
                    if (null != fis) {
                        fis.close();
                    }
                } catch (IOException ex) {
                }
            }
        }
        return buffer;
    }




    public static void getFile(byte[] bfile,String fileName) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        try {
            file = new File("mnt/sdcard/tzData/"+fileName);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}

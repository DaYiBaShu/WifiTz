package com.dianpingmedia.wifitz.read.listener;

import android.os.Handler;

/**
 * @author zsc
 * @create 2018/6/21 0021
 * @Describe
 */
public interface IReader {

    public void start(int retryTimes);

    public void stop();

    public void reboot();

    public void setHandler(Handler handler);

    public void addOnOutputListener(OnOutputListener onOutputListener);

}

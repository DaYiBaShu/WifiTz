package com.dianpingmedia.wifitz.read.listener;

/**
 * @author zsc
 * @create 2018/6/21 0021
 * @Describe
 */
public interface OnOutputListener {
    void onSuccess(String data);

    void onFail(String failMsg);
}

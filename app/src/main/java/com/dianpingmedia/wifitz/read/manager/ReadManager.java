package com.dianpingmedia.wifitz.read.manager;

import android.os.Handler;

import com.dianpingmedia.wifitz.read.listener.IReader;
import com.dianpingmedia.wifitz.read.listener.OnOutputListener;

import org.apache.tools.ant.taskdefs.condition.Or;

/**
 * @author zsc
 * @create 2018/6/21 0021
 * @Describe
 */
public abstract class ReadManager implements IReader {
    private IReader mReader;
    private boolean run;
    private boolean hasData;

    @Override
    public synchronized void start(int retryTimes) {
        mReader.start(retryTimes);
        run = true;
    }

    public synchronized void stop() {
        mReader.stop();
        run = false;
    }

    protected abstract IReader getReader();

    @Override
    public synchronized void addOnOutputListener(final OnOutputListener onOutputListener) {
        mReader.addOnOutputListener(new OnOutputListener() {
            @Override
            public void onSuccess(String data) {
                hasData = true;
                onOutputListener.onSuccess(data);
            }

            @Override
            public void onFail(String failMsg) {
                onOutputListener.onFail(failMsg);
            }
        });
    }

    @Override
    public synchronized void reboot() {
        mReader.reboot();
        run = true;
    }

    @Override
    public synchronized void setHandler(Handler handler) {
        mReader.setHandler(handler);
    }

    public ReadManager() {
        mReader = getReader();
    }

    public synchronized boolean isRunning() {
        return run && hasData;
    }
}

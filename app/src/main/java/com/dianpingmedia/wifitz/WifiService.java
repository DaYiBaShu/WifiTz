package com.dianpingmedia.wifitz;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.dianpingmedia.wifitz.dao.TzDataSaveManager;
import com.dianpingmedia.wifitz.read.listener.IReader;
import com.dianpingmedia.wifitz.read.listener.OnOutputListener;
import com.dianpingmedia.wifitz.read.manager.ReadManager;
import com.dianpingmedia.wifitz.read.usb.UsbReadManager;
import com.dianpingmedia.wifitz.receiver.UsbBroadcastReceiver;
import com.dianpingmedia.wifitz.upload.TzDataUploadManager;
import com.dianpingmedia.wifitz.utlis.SPUtils;
import com.dianpingmedia.wifitz.read.serial.SerialPortManager;

/**
 * @author zsc
 * @create 2018/3/19 0019
 * @Describe
 */
public class WifiService extends Service {
    private static String TAG = WifiService.class.getSimpleName();
    private UsbBroadcastReceiver mUsbBroadcastReceiver;
    private TzDataSaveManager mTzDataSaveManager;
    private TzDataUploadManager mTzDataUploadManager;
    private Handler mHandler;
    public static ReadManager mReadManager = new ReadManager() {
        @Override
        public IReader getReader() {
            return SerialPortManager.getInstance();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mUsbBroadcastReceiver = new UsbBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbBroadcastReceiver.OFF_ACTION);
        filter.addAction(UsbBroadcastReceiver.ON_ACTION);
        registerReceiver(mUsbBroadcastReceiver, filter);
        mTzDataSaveManager = new TzDataSaveManager(this);
        mTzDataUploadManager = new TzDataUploadManager();
        mTzDataUploadManager.startUpload();
        mReadManager.addOnOutputListener(new OnOutputListener() {
            @Override
            public void onSuccess(String data) {

                if (!TextUtils.isEmpty(data)) {
                    mTzDataSaveManager.postInsert(data);
                    if (mHandler != null) {
                        Message obtain = Message.obtain();
                        obtain.what = 0;
                        obtain.obj = data;
                        mHandler.sendMessage(obtain);
                    }
                }
            }

            @Override
            public void onFail(String failMsg) {

            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "--------onStartCommand----------");
        mReadManager.start(2);
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new WifiBinder();
    }

    @Override
    public void unbindService(ServiceConnection conn) {
        super.unbindService(conn);
        Log.i(TAG, "--------unbindService----------");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "--------onDestroy----------");
        if (mUsbBroadcastReceiver != null) {
            unregisterReceiver(mUsbBroadcastReceiver);
        }
        if (mReadManager != null) {
            mReadManager.stop();
        }
        if (mTzDataUploadManager != null) {
            mTzDataUploadManager.release();
        }
        if (mTzDataSaveManager != null) {
            mTzDataSaveManager.release();
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    public class WifiBinder extends Binder {
        public WifiService getService() {
            return WifiService.this;
        }
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
        mReadManager.setHandler(handler);
        mUsbBroadcastReceiver.setHandler(handler);
        mTzDataSaveManager.setHandler(handler);
        mTzDataUploadManager.setHandler(handler);
    }

    public void reboot() {
        mReadManager.reboot();
        if (mTzDataUploadManager != null) {
            mTzDataUploadManager.startUpload();
        }
    }

    public void start() {
        if (!mReadManager.isRunning()) {
            mReadManager.stop();
            mReadManager.start(2);
        }
    }

    private void sendLog(String log) {
        if (!TextUtils.isEmpty(log)) {
            Log.i(TAG, log);
            if (mHandler != null) {
                Message obtain = Message.obtain();
                obtain.what = 0;
                obtain.obj = log;
                mHandler.sendMessage(obtain);
            }
        }
    }
}
